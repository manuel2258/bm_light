#pragma once

#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

#include "websocket.h"

typedef struct {
  QueueHandle_t queue;
  esp_websocket_client_handle_t ws_client;
} DisplayHandleGroup;

void display_run(void *args);