#pragma once

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"

#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT BIT1

void wifi_init_connection(EventGroupHandle_t *event_group);

bool wifi_wait_for_connection(EventGroupHandle_t *event_group);