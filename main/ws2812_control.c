#include "ws2812_control.h"
#include "driver/rmt.h"

#include "esp_log.h"

#define NUM_LEDS 256

#define LED_RMT_TX_CHANNEL 0
#define LED_RMT_TX_GPIO 18

#define LED_BUFFER_ITEMS ((NUM_LEDS * 3 * 8))

// These values are determined by measuring pulse timing with logic analyzer and
// adjusting to match datasheet.
#define T0H 14 // 0 bit high time
#define T1H 52 // 1 bit high time
#define TL 52  // low time for either bit

rmt_item32_t led_data_buffer[LED_BUFFER_ITEMS];

void ws2812_control_init(void) {
  rmt_config_t config;
  config.rmt_mode = RMT_MODE_TX;
  config.channel = LED_RMT_TX_CHANNEL;
  config.gpio_num = LED_RMT_TX_GPIO;
  config.mem_block_num = 3;
  config.tx_config.loop_en = false;
  config.tx_config.carrier_en = false;
  config.tx_config.idle_output_en = true;
  config.tx_config.idle_level = 0;
  config.clk_div = 2;

  ESP_ERROR_CHECK(rmt_config(&config));
  ESP_ERROR_CHECK(rmt_driver_install(config.channel, 0, 0));
}

void fill_rmt_data_buffer(uint8_t *buffer) {
  for (uint32_t led = 0; led < (NUM_LEDS * 3); led++) {
    uint8_t data_byte = buffer[led];
    for (uint8_t bit = 0; bit < 8; bit++) {
      uint8_t mask = 1 << (7 - bit);
      bool bit_is_set = (data_byte & mask) != 0;
      led_data_buffer[(led * 8) + bit] =
          bit_is_set ? (rmt_item32_t){{{T1H, 1, TL, 0}}}
                     : (rmt_item32_t){{{T0H, 1, TL, 0}}};
    }
  }
}

void ws2812_write_leds(uint8_t *buffer) {
  fill_rmt_data_buffer(buffer);
  ESP_ERROR_CHECK(rmt_write_items(LED_RMT_TX_CHANNEL, led_data_buffer,
                                  LED_BUFFER_ITEMS, false));
  ESP_ERROR_CHECK(rmt_wait_tx_done(LED_RMT_TX_CHANNEL, portMAX_DELAY));
}
