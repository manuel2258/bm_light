idf_component_register(
    SRCS 
        "main.c" 
        "wifi.c"
        "websocket.c"
        "display.c"
        "ws2812_control.c"
    INCLUDE_DIRS "."
)
