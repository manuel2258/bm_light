#pragma once

#include <stdint.h>

void ws2812_control_init(void);

void ws2812_write_leds(uint8_t *buffer);