#include "websocket.h"

#include "esp_event.h"
#include "esp_system.h"
#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

#include "esp_event.h"
#include "esp_log.h"

#include "config.h"

#define NO_DATA_TIMEOUT_SEC 10

static const char *TAG = "WEBSOCKET";

void websocket_event_handler(void *args, esp_event_base_t base,
                             int32_t event_id, void *event_data) {
  WebsocketHandleGroup *handles = (WebsocketHandleGroup *)args;
  esp_websocket_event_data_t *data = (esp_websocket_event_data_t *)event_data;
  switch (event_id) {
  case WEBSOCKET_EVENT_CONNECTED:
    xEventGroupSetBits(handles->event_group, WEBSOCKET_CONNECTED_BIT);
    break;
  case WEBSOCKET_EVENT_DISCONNECTED:
    xEventGroupSetBits(handles->event_group, WEBSOCKET_DISCONNECTED_BIT);
    break;
  case WEBSOCKET_EVENT_DATA:
    if (data->op_code == 0x08 && data->data_len == 2) {
      ESP_LOGW(TAG, "Received closed message with code=%d",
               256 * data->data_ptr[0] + data->data_ptr[1]);
    } else if (data->op_code == 0x02) {
      esp_websocket_event_data_t *data_cpy =
          malloc(sizeof(esp_websocket_event_data_t));
      memcpy(data_cpy, data, sizeof(esp_websocket_event_data_t));

      data_cpy->data_ptr = malloc(sizeof(uint8_t) * data->data_len);
      memcpy((void *)data_cpy->data_ptr, (void *)data->data_ptr,
             data->data_len);

      if (xQueueGenericSend(handles->queue, (void *)&data_cpy, portMAX_DELAY,
                            queueSEND_TO_BACK) != pdPASS) {
        ESP_LOGE(TAG, "Could not append data to queue ...");
      } else {
        ESP_LOGI(TAG, "Appended data msg at %p to display queue",
                 (void *)data_cpy);
      }
    } else if (data->op_code == 0x0a) {
      ESP_LOGI(TAG, "Received pong");
    } else {
      ESP_LOGE(TAG, "Received unexpected msg: opcode=%hhu len=%d msg=%.*s",
               data->op_code, data->data_len, data->data_len,
               (char *)data->data_ptr);
    }
    break;
  case WEBSOCKET_EVENT_ERROR:
    xEventGroupSetBits(handles->event_group, WEBSOCKET_ERROR_BIT);
    break;
  }
}

void websocket_send_ok(esp_websocket_client_handle_t client) {
  if (esp_websocket_client_is_connected(client)) {
    char data[20];
    int len = sprintf(data, "{ \"answer\": \"ok\" }\n");
    ESP_LOGI(TAG, "Sending %s", data);
    esp_websocket_client_send_text(client, data, len, portMAX_DELAY);
  }
}

void websocket_send_not_ok(esp_websocket_client_handle_t client) {
  if (esp_websocket_client_is_connected(client)) {
    char data[38];
    int len = sprintf(data, "{ \"answer\": \"invalid data length\" }\n");
    ESP_LOGI(TAG, "Sending %s", data);
    esp_websocket_client_send_text(client, data, len, portMAX_DELAY);
  }
}

esp_websocket_client_handle_t websocket_init(WebsocketHandleGroup *handles) {
  esp_websocket_client_config_t websocket_cfg = {};

  websocket_cfg.uri = WEBSOCKET_URI;

  ESP_LOGI(TAG, "Connecting to %s...", websocket_cfg.uri);

  esp_websocket_client_handle_t client =
      esp_websocket_client_init(&websocket_cfg);
  esp_websocket_register_events(client, WEBSOCKET_EVENT_ANY,
                                websocket_event_handler, (void *)handles);

  esp_websocket_client_start(client);

  return client;
}

bool websocket_wait_for_connection(EventGroupHandle_t *event_group) {
  EventBits_t bits = xEventGroupWaitBits(
      *event_group,
      WEBSOCKET_CONNECTED_BIT | WEBSOCKET_ERROR_BIT | WEBSOCKET_ERROR_BIT,
      pdFALSE, pdFALSE, portMAX_DELAY);

  if (bits & WEBSOCKET_CONNECTED_BIT) {
    ESP_LOGI(TAG, "Connected to websocket: %s", WEBSOCKET_URI);
    return true;
  } else if (bits & WEBSOCKET_DISCONNECTED_BIT || bits & WEBSOCKET_ERROR_BIT) {
    ESP_LOGE(TAG, "Error while connecting to websocket: %s", WEBSOCKET_URI);
    return false;
  } else {
    ESP_LOGE(TAG, "UNEXPECTED EVENT");
    return false;
  }
}

void websocket_close(esp_websocket_client_handle_t client) {
  esp_websocket_client_close(client, portMAX_DELAY);
  ESP_LOGI(TAG, "Websocket Stopped");
  esp_websocket_client_destroy(client);
}