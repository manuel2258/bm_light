#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "nvs_flash.h"

#include "display.h"
#include "websocket.h"
#include "wifi.h"
#include "ws2812_control.h"

const int ALL_BITS = WIFI_CONNECTED_BIT | WIFI_FAIL_BIT |
                     WEBSOCKET_CONNECTED_BIT | WEBSOCKET_DISCONNECTED_BIT |
                     WEBSOCKET_ERROR_BIT;

static const char *TAG = "MAIN";

void init_nvs() {
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES ||
      ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret);
}

void app_main(void) {
  init_nvs();

  EventGroupHandle_t event_group = xEventGroupCreate();
  QueueHandle_t queue = xQueueCreate(20, sizeof(esp_websocket_event_data_t *));
  WebsocketHandleGroup ws_handles = {
      .event_group = event_group,
      .queue = queue,
  };

  wifi_init_connection(&event_group);
  wifi_wait_for_connection(&event_group);

  esp_websocket_client_handle_t ws_client = websocket_init(&ws_handles);
  websocket_wait_for_connection(&event_group);

  DisplayHandleGroup display_handles = {
      .queue = queue,
      .ws_client = ws_client,
  };

  TaskHandle_t display_task;
  xTaskCreatePinnedToCore(display_run, "DISPLAY", 4096,
                          (void *)&display_handles, tskIDLE_PRIORITY,
                          &display_task, 1);
  configASSERT(display_task);

  for (;;) {
    EventBits_t bits =
        xEventGroupWaitBits(event_group, ALL_BITS, pdTRUE, pdFALSE, 100);

    if (bits & WIFI_CONNECTED_BIT) {
    } else if (bits & WIFI_FAIL_BIT) {
    }
  }
}
