#include "display.h"

#include "ws2812_control.h"

static const char *TAG = "DISPLAY";

void display_run(void *args) {
  DisplayHandleGroup *handles = (DisplayHandleGroup *)args;

  esp_websocket_event_data_t *ws_data;

  ws2812_control_init();
  uint8_t *buffer = malloc(sizeof(uint8_t) * 256 * 3);
  for (int i = 0; i < 256 * 3; ++i) {
    buffer[i] = 0;
  }
  ws2812_write_leds(buffer);
  free(buffer);

  ESP_LOGI(TAG, "Initialized LED's, starting display loop");
  for (;;) {
    if (xQueueReceive(handles->queue, &ws_data, 100)) {
      ESP_LOGI(TAG, "Received(%d) opcode=%hhu msg=%.*s", ws_data->data_len,
               ws_data->op_code, ws_data->data_len, (char *)ws_data->data_ptr);
      if (ws_data->op_code == 0x02) {
        if (ws_data->data_len != 256 * 3) {
          ESP_LOGE(TAG,
                   "Received data with invalid length, returning not okey!");
          websocket_send_not_ok(handles->ws_client);
        } else {
          ws2812_write_leds((uint8_t *)ws_data->data_ptr);
          ESP_LOGI(TAG,
                   "Received valid data, updated display and sending okey!");
          websocket_send_ok(handles->ws_client);
        }
      }
      free((void *)ws_data->data_ptr);
      free((void *)ws_data);
    }
  }
}