#pragma once

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"

#include "esp_websocket_client.h"

#define WEBSOCKET_CONNECTED_BIT BIT2
#define WEBSOCKET_DISCONNECTED_BIT BIT3
#define WEBSOCKET_ERROR_BIT BIT4

typedef struct {
  EventGroupHandle_t event_group;
  QueueHandle_t queue;
} WebsocketHandleGroup;

esp_websocket_client_handle_t websocket_init(WebsocketHandleGroup *handles);
bool websocket_wait_for_connection(EventGroupHandle_t *event_group);
void websocket_close(esp_websocket_client_handle_t client);

void websocket_send_ok(esp_websocket_client_handle_t client);
void websocket_send_not_ok(esp_websocket_client_handle_t client);
